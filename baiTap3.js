/* 
*Input: 
- Nhập vào số tiền USD 

*Step:
-S1 : Tạo biến cho USD và tỷ giá của VND hiện tại 
-S2 : Gán giá trị cho tý giá của VND hiện tại
-S3 : Cách tính USD = số tiền USD * tý giá của VND 
-S4 : Xuất ra màn hình số tiền sau khi quy đổi từ USD qua VND 

*Output:
- Số tiền sau khi quy đổi từ USD sang VND

*/

var quyDoi = function () {
  var tienDoValue = document.getElementById("uSD").value;
  console.log(tienDoValue);
  var tyGia = 23500;
  var quyDoi = tienDoValue * tyGia;
  console.log({ quyDoi });
  document.getElementById(
    "tongVND"
  ).innerHTML = `<p>Tổng tiền quy đổi được là : ${quyDoi}VND</p>`;
};
