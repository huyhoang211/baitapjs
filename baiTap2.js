/* 
*Input: 
- Nhập vào 5 số thực bất kỳ
*Step :
-S1 : Tạo biến và gán giá trị cho các số thực được nhập vào
-S2 : Tính giá trị trung bình của các số thực được nhập vào = Tổng các số nhập vào chia đều cho 5 
-S3 : In kết quả giá trị trung bình ra console
*Output : 
- Giá trị trung bình của 5 số
*/

var dtb = function () {
  var monMotValue = parseInt(document.getElementById("monMot").value);
  var monHaiValue = parseInt(document.getElementById("monHai").value);
  var monBaValue = parseInt(document.getElementById("monBa").value);
  var monBonValue = parseInt(document.getElementById("monBon").value);
  var monNamValue = parseInt(document.getElementById("monNam").value);
  console.log(monMotValue, monHaiValue, monBaValue, monBonValue, monNamValue);
  var trungbinh =
    (monMotValue + monHaiValue + monBaValue + monBonValue + monNamValue) / 5;
  console.log({ trungbinh });
  document.getElementById(
    "trungBinhCong"
  ).innerHTML = `<p> Trung bình cộng 5 sô của bạn là: ${trungbinh}</p>`;
};
