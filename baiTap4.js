/* 
*Input: 
- 2 cạnh chiều dài = 3cm
- 2 cạnh chiều rộng = 4cm

*Step: 
-S1 : tạo 2 biến lưu 2 input
-S2 : tạo 2 biến lưu  2 output
-S3 : áp dụng công thức tính chu vi và diện tích hình chữ nhật 

*Output : 
- Chu Vi = 14 
- Dien Tich = 12 

*/
var cvdt = function () {
  var chieuDaiValue = parseInt(document.getElementById("chieuDai").value);
  var chieuRongValue = parseInt(document.getElementById("chieuRong").value);
  console.log(chieuDaiValue, chieuRongValue);
  var chuVi = (chieuDaiValue + chieuRongValue) * 2;
  var dienTich = chieuDaiValue * chieuRongValue;
  console.log(chuVi);
  console.log(dienTich);
  document.getElementById(
    "tongCvdt"
  ).innerHTML = `<p>Hình chữ nhật của bạn có chu vi là : ${chuVi} và diện tích là : ${dienTich}</p>`;
};
