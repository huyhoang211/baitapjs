/* Input:
- Số nguyên có 2 chữ số
- 41 = 4 + 1 = 5

*Step:
- S1 : tạo biến chục, đơn vị, input , output.
- S2 : gán giá trị cho Input
- S3 : tách số hàng đơn vị -> n % 10 = 1
- S4 : tách số hàng chục -> n /10 = 4 
- S5 : in kết quả sum ra console 

*Output: 
- Kết quả sum = 5 
*/

var tt = function () {
  var soNguyenValue = document.getElementById("soNguyen").value;
  console.log(soNguyenValue);
  var donVi = soNguyenValue % 10;
  var chuc = parseInt(soNguyenValue / 10);
  console.log(chuc, donVi);
  var sum = chuc + donVi;
  console.log(sum);
  document.getElementById(
    "ketQua"
  ).innerHTML = `<p>Số hàng chục là : ${chuc}, đơn vị là : ${donVi} và tổng của 2 số là : ${sum}</p>`;
};
