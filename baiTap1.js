/*
*Input :
- Lương 1 ngày = 100.000đ
- Số ngày đi làm

*Step :
-S1 : Tạo biến cho số ngày đi làm và lương 1 ngày 
-S2 : Áp dụng cách tính lương = lương 1 ngày x số ngày đi làm 

*Output:
- Tiền lương nhân viên theo số ngày đi làm nhập vào
 */

var tongTien = function () {
  var ngayLamViecValue = document.getElementById("ngay_lam").value;

  console.log(ngayLamViecValue);

  var luongMotNgay = 100000;

  var tongLuong = luongMotNgay * ngayLamViecValue;

  console.log({ tongLuong });

  document.getElementById(
    "tongLuong"
  ).innerHTML = `<p>Tổng lương tháng này của bạn là : ${tongLuong}VND</p>`;
};
